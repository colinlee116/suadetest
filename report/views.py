from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from .models import Report
import json
import dicttoxml

from django.views.generic.detail import SingleObjectMixin
from easy_pdf.views import PDFTemplateView

def view_all_reports(request):
    template_name = 'report/view_all_reports.html'
    reports = Report.objects.all()
    context = {
        'reports': reports
    }
    return render(request, template_name, context)


def view_report(request, report_id):
    template_name = 'report/view_report.html'
    report = get_object_or_404(Report, id=report_id)
    context = {
        'report': report
    }
    return render(request, template_name, context)

def view_xml_report(request, report_id):
    report = get_object_or_404(Report, id=report_id)
    data = json.loads(report.type)
    xml = dicttoxml.dicttoxml(data)
    return HttpResponse(xml, content_type='text/xml')

def view_pdf_report(request, report_id):
    report = get_object_or_404(Report, id=report_id)
    data = json.loads(report.type)
    xml = dicttoxml.dicttoxml(data)
    return HttpResponse(xml, content_type='text/xml')

class ReportPdfView(SingleObjectMixin, PDFTemplateView):

    template_name = "report/report_pdf_template.html"
    queryset = Report.objects.all()

    def get_object(self):
        report = get_object_or_404(Report, id=self.kwargs.get('report_id', None))
        return report

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super(ReportPdfView, self).get_context_data(**kwargs)
        context['pagesize'] = "A4"
        report = json.loads(self.object.type)
        context['report'] = report
        return context

view_pdf_report = ReportPdfView.as_view()