from __future__ import unicode_literals
from django.contrib.postgres.fields import JSONField

from django.db import models

# Create your models here.

class Report(models.Model):
    type = models.TextField()

    class Meta:
        managed = False
        db_table = 'reports'
