from django.conf.urls import include, url
from report import views

urlpatterns = [
    url(r'^reports/$', views.view_all_reports, name='report_all'),

    url(
        r'^report/(?P<report_id>[-\w\d]+)/$',
        views.view_report,
        name='view_report'
    ),

    url(
        r'^report/xml/(?P<report_id>[-\w\d]+)/$',
        views.view_xml_report,
        name='view_xml_report'
    ),

    url(
        r'^report/pdf/(?P<report_id>[-\w\d]+)/$',
        views.view_pdf_report,
        name='view_pdf_report'
    ),
]